//
//  main.m
//  Tag Organizer
//
//  Created by Erik Engheim on 1/20/13.
//  Copyright (c) 2013 Erik Engheim. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
  return NSApplicationMain(argc, (const char **)argv);
}
