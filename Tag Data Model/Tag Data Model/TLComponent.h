//
//  TLComponent.h
//  Tag Data Model
//
//  Created by Erik Engheim on 1/20/13.
//  Copyright (c) 2013 Erik Engheim. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TLComponent <NSObject>
@property (nonatomic, strong) NSDictionary *attributes;
@property (nonatomic, copy) NSString *iconName;
@end
