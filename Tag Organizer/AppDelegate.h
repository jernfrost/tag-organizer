//
//  AppDelegate.h
//  Tag Organizer
//
//  Created by Erik Engheim on 1/20/13.
//  Copyright (c) 2013 Erik Engheim. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
