//
//  NSDictionary+SetOperations.m
//  Tag Data Model
//
//  Created by Erik Engheim on 2/19/13.
//  Copyright (c) 2013 Erik Engheim. All rights reserved.
//

#import "NSMutableDictionary+SetOperations.h"

@implementation NSMutableDictionary (SetOperations)

- (void)intersectKeys:(NSArray *)keys
{
    for (NSString *key in keys) {
        if (!self[key])
            [self removeObjectForKey:key];
    }
}
@end
