Data Model
==========
Data is organized into multiple overlapping sets, instead of a hierarchy of folders. An alternative way of looking at this is think of each data object as being tagged with zero or more tags. The tags corresponds to the sets. 

The tags or sets are represented in code by the ``TLTagSet`` base class. Every data object is owned by one and only one set. This is the ``TLTypeTagSet``. All the other sets keep weak references. Objective-C ARC will make sure that weak references are set to ``nil`` when the reference count reaches zero. A benefit of this is that we do not need to design any kind of listening or notification mechanism, so that owning sets have to tell the others when their objects get deleted.

Different kinds of sets
-----------------------
There are 3 different kinds of sets:

 * **Type Set** implemented by ``TLTagSet`` subclass ``TLTypeTagSet``. There is one set for each kind of data. Owns objects, through strong reference.
 * **User Set** users can freely add or remove objects manually. Does not own object. Uses weak reference.
 * **Predicate Set** membership is decided based on whether predicate evaluates to TRUE for object. Holds objects through weak reference.

Users will typically not be able to add freely any kind of object to *type sets*. Every time an object of a particular type is created it will be added to its corresponding typeset. *User sets* on the other hand may contain any kind of data object. For this reason *type sets* are implemented using ``NSMutableSet`` while *user sets* and *predicate sets* are implemented using ``NSHashTable`` which holds weak references to its members.

UI representations
------------------
The data objects will not be in a fixed order in the UI, only the sets. The reason is that the data objects one sees in the object list could be a combination of objects from multiple sets. There would be no practical way for the user to define the order of objects for every combination of sets.

Instead the list of objects may be sorted by any of its attributes. If you talk of bugs then one of the attributes might be the date the bug was filed or reported. Sorting on this date would give the order one would usually find in more traditional data explorers.

Attributes available for predicates and UI
------------------------------------------
Need to add set operation such as union and intersection to ``NSDictionary`` class. This is so that when mixing objects of different type we can find their common attributes. Only the common attributes will show up in table columns displaying data objects. And only the common attributes will be available when defining *predicate filters*.

Data objects (Components)
-------------------------
The actual data is contained within data objects of type ``TLComponent``. We should experiment with having ``TLComponent`` be either a base class or a protocol. The advantage of using a protocol is that if Tag Organizer is a generic framework then people should be able to plug in their existing objects which may not inherit ``TLComponent``. 

``TLComponent`` needs to support key-value coding for its properties, to work with predicates. Further it needs a way to be queried about which properties it has and their type. Possible models for this would be Core Image and the ``CIFilter`` class.

``TLComponent`` should have a ``attributes`` method returning a dictionary describing the properties found on a component, accessible through key-value coding. Each key gives the name of the attribute.

	{
		"measuredDepth" = {
			"type" = NSNumber,
			"uiName" = "Measured Depth"
			"default" = 0,
			"min" = 0
			"max" = 4000
			},
		
		"name" = {
			"type" = NSString
			"uiName" = "name"
			"default" = "unknown"
		}
	}

Predicate Editor
================
``NSPredicateEditor`` uses an array of ``NSPredicateEditorRowTemplate`` which can be added and configured in code or interface builder. The actual rows are based on the templates. The number of rows in interface builder is not the same as the number of rows at runtime. At the design time there is a row for each type of row template.

The Predicate editor builds the GUI from a predicate by calling ``matchForPredicate:`` on each row template. The template which gives the higest row number is selected. ``predicateWithSubpredicates:`` can be overriden to change how a predicate if calculated from a row.

You set the row templates programatically with ``setRowTemplates:``