//
//  TLTagObj.h
//  Tag Data Model
//
//  Created by Erik Engheim on 1/20/13.
//  Copyright (c) 2013 Erik Engheim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLComponent.h"

@interface TLTagSet : NSObject
{
    NSMutableSet *_comps;             // actual data
    NSMutableArray *_children;   // child sets
}

- (id)initWithName:(NSString *)name;

@property (nonatomic, copy) NSString *name;

#pragma mark adding and removing
- (void)addComp:(id<TLComponent>)object;
- (void)removeComp:(id<TLComponent>)object;

#pragma mark access content
- (BOOL)containsComp:(id<TLComponent>)anObject;
- (id<TLComponent>)member:(id<TLComponent>)object;
- (NSUInteger)compCount;
- (NSEnumerator *)objectEnumerator;

#pragma mark child sets
- (NSUInteger)childCount;
- (void)addChild:(TLTagSet *)child;
- (void)insertChild:(TLTagSet *)child atIndex:(NSUInteger)index;
- (TLTagSet *)childAtIndex:(NSUInteger)index;

#pragma mark set operations
- (void)unionTagSet:(TLTagSet *)other;
- (void)minusTagSet:(TLTagSet *)other;
- (void)intersectTagSet:(TLTagSet *)other;

@end
