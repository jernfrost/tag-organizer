//
//  TLTagObj.m
//  Tag Data Model
//
//  Created by Erik Engheim on 1/20/13.
//  Copyright (c) 2013 Erik Engheim. All rights reserved.
//

#import "TLTagSet.h"
#import "NSMutableDictionary+SetOperations.h"

@implementation TLTagSet
- (id)initWithName:(NSString *)name
{
  self = [super init];
  if (self) {
      _name     = name;
      _comps    = [[NSMutableSet alloc] initWithCapacity:5];
      _children = [[NSMutableArray alloc] initWithCapacity:5];
  }
  return self;
}

#pragma mark adding and removing
- (void)addComp:(id<TLComponent>)object
{
    [_comps addObject:object];
}

- (void)removeComp:(id<TLComponent>)object
{
    [_comps removeObject:object];
}

#pragma mark access content
- (BOOL)containsComp:(id<TLComponent>)anObject
{
    return [_comps containsObject:anObject];
}

- (id<TLComponent>)member:(id<TLComponent>)object
{
    return [_comps member:object];
}

- (NSUInteger)compCount
{
    return _comps.count;
}

- (NSEnumerator *)objectEnumerator
{
    return [_comps objectEnumerator];
}

#pragma mark child sets
- (NSUInteger)childCount
{
    return _children.count;
}

- (void)addChild:(TLTagSet *)child
{
    [_children addObject:child];
}
- (void)insertChild:(TLTagSet *)child atIndex:(NSUInteger)index
{
    [_children insertObject:child atIndex:index];
}

- (TLTagSet *)childAtIndex:(NSUInteger)index;
{
    return _children[index];
}


#pragma mark set operations
- (void)unionTagSet:(TLTagSet *)other
{
    [_comps unionSet:other->_comps];

}

- (void)minusTagSet:(TLTagSet *)other
{
    [_comps minusSet:other->_comps];
}

- (void)intersectTagSet:(TLTagSet *)other
{
    [_comps intersectSet:other->_comps];
}

- (NSDictionary *)sharedAttributes
{
    id<TLComponent> comp = _comps.anyObject;
    NSMutableDictionary *attributes = comp.attributes.mutableCopy;
    if (attributes) {
        for (id<TLComponent> comp in _comps) {
            [attributes intersectKeys:comp.attributes.allKeys];
        }
    }
    
    return attributes;
}
@end
