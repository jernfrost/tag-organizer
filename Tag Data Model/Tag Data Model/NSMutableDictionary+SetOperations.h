//
//  NSDictionary+SetOperations.h
//  Tag Data Model
//
//  Created by Erik Engheim on 2/19/13.
//  Copyright (c) 2013 Erik Engheim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (SetOperations)
- (void)intersectKeys:(NSArray *)keys;
@end
